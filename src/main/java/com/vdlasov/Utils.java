package com.vdlasov;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Denis on 22.02.2017.
 */
public class Utils {
    // "2017-02-21"
    public static long dateDiff(String first, String second) {
        SimpleDateFormat sdf = getSimpleDateFormat();
        try {
            Date f = sdf.parse(first);
            Date s = sdf.parse(second);

            long diffTime = s.getTime() - f.getTime();
            return diffTime / (1000 * 60 * 60 * 24);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long dateDiffWithNow(String date) {
        SimpleDateFormat sdf = getSimpleDateFormat();
        return dateDiff(date, sdf.format(new Date()));
    }

    public static long dateDiffWithNow(Date date) {
        SimpleDateFormat sdf = getSimpleDateFormat();
        return dateDiff(sdf.format(date), sdf.format(new Date()));
    }

    private static SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }
}
