package com.vdlasov.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Denis on 20.02.2017.
 */
public enum Currency {
    AUD, BGN, BRL, CAD, CHF, CNY,
    CZK, DKK, GBP, HKD, HRK, HUF,
    IDR, ILS, INR, JPY, KRW, MXN,
    MYR, NOK, NZD, PHP, PLN, RON,
    RUB, SEK, SGD, THB, TRY, USD,
    ZAR;

    Currency() {
        Holder.MAP.put(this.name(), this);
    }

    public static Currency find(String val) {
        Currency c = Holder.MAP.get(val);
        if (c == null) {
            throw new IllegalStateException(String.format("Unsupported type %s.", val));
        }
        return c;
    }

    public static boolean isExist(String val) {
        Currency c = Holder.MAP.get(val);
        if (c == null) {
            return false;
        }
        return true;
    }

    // thanks! http://stackoverflow.com/a/27807324/3002121
    // use this code for search string name in enums
    private static class Holder {
        static Map<String, Currency> MAP = new HashMap<>();
    }
}
