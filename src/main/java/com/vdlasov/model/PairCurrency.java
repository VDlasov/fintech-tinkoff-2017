package com.vdlasov.model;

import java.util.Objects;

/**
 * Created by Denis on 21.02.2017.
 */
public class PairCurrency {
    // TODO refactor first->from, second->to
    private Currency first;
    private Currency second;

    public PairCurrency(Currency first, Currency second) {
        this.first = first;
        this.second = second;
    }

    public PairCurrency(String first, String second) {
        this.first = Currency.valueOf(first);
        this.second = Currency.valueOf(second);
    }

    public Currency getFirst() {
        return first;
    }

    public Currency getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PairCurrency that = (PairCurrency) o;
        return first == that.first &&
                second == that.second;
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
}
