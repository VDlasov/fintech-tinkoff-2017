package com.vdlasov.model.network;

/**
 * Created by Denis on 20.02.2017.
 */
public class ApiResponse {
    private String base;
    private String date;
    private RateObject rates;

    public String getBase() {
        return base;
    }

    public RateObject getRates() {
        return rates;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "base='" + base + '\'' +
                ", rates=" + rates +
                '}';
    }
}

