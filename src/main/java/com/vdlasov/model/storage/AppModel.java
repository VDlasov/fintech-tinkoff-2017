package com.vdlasov.model.storage;

import com.vdlasov.model.PairCurrency;
import com.vdlasov.model.network.ApiResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Denis on 22.02.2017.
 */
public class AppModel {
    private Map<PairCurrency, ApiResponse> quotes = new HashMap<>();

    public Map<PairCurrency, ApiResponse> getQuotes() {
        return quotes;
    }
}
