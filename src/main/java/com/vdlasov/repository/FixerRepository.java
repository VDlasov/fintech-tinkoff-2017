package com.vdlasov.repository;

import com.vdlasov.api.FixerApi;
import com.vdlasov.model.Currency;
import com.vdlasov.model.PairCurrency;
import com.vdlasov.model.network.ApiResponse;
import com.vdlasov.persistent.PersistentStorage;
import com.vdlasov.ui.ProgressBar;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * Created by Denis on 21.02.2017.
 */
public class FixerRepository {
    private final FixerApi api = new FixerApi();
    private final PersistentStorage storage = new PersistentStorage();
    private final Logger log = Logger.getGlobal();
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public ApiResponse getCurrency(Currency from, Currency to) throws ExecutionException, InterruptedException {

        Future future = executorService.submit(() -> {
            log.fine("Asynchronous Callable");

            ApiResponse result = null;

            // load from storage
            //storage.loadFromDisk();
            result = storage.find(new PairCurrency(from, to));

            if (result == null) {
                result = api.getCurrency(from, to);
                log.info("load data from network");

                // save to storage
                storage.put(new PairCurrency(from, to), result);
                //storage.saveToDisk();
            }


            return result;
        });

        // show progress bar = == === ====
        new ProgressBar(future).start();

        return (ApiResponse) future.get();
    }

    public void destroy() {
        executorService.shutdown();
    }
}
