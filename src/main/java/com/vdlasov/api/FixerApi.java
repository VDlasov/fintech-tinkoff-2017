package com.vdlasov.api;

import com.vdlasov.di.DI;
import com.vdlasov.model.Currency;
import com.vdlasov.model.network.ApiResponse;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Denis on 20.02.2017.
 */
// Work with rest api
public class FixerApi {
    public ApiResponse getCurrency(Currency from, Currency to) {
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL("http://api.fixer.io/latest?base=" + from + "&symbols=" + to);

            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            Reader reader = new InputStreamReader(in, "UTF-8");
            return DI.getGsonApi().fromJson(reader, ApiResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
}
