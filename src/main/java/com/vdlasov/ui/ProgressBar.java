package com.vdlasov.ui;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

/**
 * Created by Denis on 21.02.2017.
 */
public class ProgressBar {
    private final Logger log = Logger.getGlobal();
    private final Future future;

    public ProgressBar(Future future) {
        this.future = future;
    }

    // thanks! http://stackoverflow.com/questions/852665/command-line-progress-bar-in-java
    private static String DisplayBar(int i) {
        StringBuilder sb = new StringBuilder();

        int x = i / 2;
        sb.append("|");
        for (int k = 0; k < 50; k++)
            sb.append(String.format("%s", ((x <= k) ? " " : "=")));
        sb.append("|");

        return sb.toString();
    }

    public void start() throws ExecutionException, InterruptedException {
        // Progress bar, show loading
        StringBuilder spaces = new StringBuilder();
        for (int i = 0; i < 20; i++) {
            spaces.append(' ');
        }

        outer:
        while (true) {
            for (int i = 1; i <= 100; i++) {
                try {
                    if (future.get(1, TimeUnit.MILLISECONDS) != null) {
                        break outer;
                    }
                } catch (TimeoutException e) {
                    log.fine(e.getClass().getSimpleName());
                }
                Thread.sleep(20);
                System.out.printf("\r%s", DisplayBar(i));
            }

            System.out.print("\r" + spaces.toString());

        }
        System.out.println("\r" + spaces.toString());
    }
}
