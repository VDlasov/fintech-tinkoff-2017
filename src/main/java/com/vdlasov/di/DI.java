package com.vdlasov.di;

import com.vdlasov.api.RatesDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vdlasov.model.network.RateObject;

/**
 * Created by Denis on 20.02.2017.
 */
public class DI {
    private static Gson gsonApi = new GsonBuilder()
            .registerTypeAdapter(RateObject.class, new RatesDeserializer()) // special deserialize case
            .create();
    private static Gson gson = new GsonBuilder()
            .enableComplexMapKeySerialization()
            //.setPrettyPrinting() // nice debug formatting
            .create();

    public static Gson getGsonApi() {
        return gsonApi;
    }

    public static Gson getGson() {
        return gson;
    }
}
