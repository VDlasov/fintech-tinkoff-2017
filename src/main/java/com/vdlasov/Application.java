package com.vdlasov;

import com.vdlasov.controller.CurrencyController;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Denis on 20.02.2017.
 */
public class Application {
    public static void main(String[] args) {
        //set log level
        Logger.getGlobal().setLevel(Level.WARNING);
        new CurrencyController().start();
    }
}
