package com.vdlasov.persistent;

import com.google.gson.JsonSyntaxException;
import com.vdlasov.Utils;
import com.vdlasov.di.DI;
import com.vdlasov.model.PairCurrency;
import com.vdlasov.model.network.ApiResponse;
import com.vdlasov.model.storage.AppModel;

import java.io.*;
import java.util.logging.Logger;

/**
 * Created by Denis on 21.02.2017.
 */
public class PersistentStorage {
    private static final String FILENAME = "currency.txt";
    private final Logger log = Logger.getGlobal();
    private AppModel appModel = new AppModel();

    private void saveToDisk() {
        if (appModel != null) {
            deleteFileFromDisk();
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
                String content = DI.getGson().toJson(appModel);
                bw.write(content);
                log.info("Save data to: " + FILENAME);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadFromDisk() {
        File file = new File(FILENAME);
        if (file.exists()) {
            StringBuilder sb = new StringBuilder();
            try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Load data from: " + FILENAME);

            try {
                appModel = DI.getGson().fromJson(sb.toString(), AppModel.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        // if old delete
    }

    // delete file from disk
    private void deleteFileFromDisk() {
        File file = new File(FILENAME);
        if (file.exists()) {
            file.delete();
        }
    }

    // search pair and return ApiResponse com.vdlasov.model
    public ApiResponse find(PairCurrency pairCurrency) {
        loadFromDisk();
        if (appModel != null && appModel.getQuotes() != null) {
            try {
                // if old date in currency delete all file
                if (Utils.dateDiffWithNow(appModel.getQuotes().get(pairCurrency).getDate()) > 1) {
                    // delete on disk
                    deleteFileFromDisk();
                    // delete in memory
                    appModel.getQuotes().clear();
                    log.info("Old cache, delete file: " + FILENAME);
                    return null;
                }
                return appModel.getQuotes().get(pairCurrency);
            } catch (NullPointerException e) {
            }
        }
        return null;
    }

    // put in map with key: PairCurrency and value ApiResponse
    public void put(PairCurrency pairCurrency, ApiResponse apiResponse) {
        if (find(pairCurrency) == null) {
            appModel.getQuotes().put(pairCurrency, apiResponse);
        } else {
            // newer object
            appModel.getQuotes().replace(pairCurrency, apiResponse);
        }
        saveToDisk();
    }

}
