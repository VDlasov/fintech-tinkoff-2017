package com.vdlasov.controller;

import com.vdlasov.model.Currency;
import com.vdlasov.model.PairCurrency;
import com.vdlasov.model.network.ApiResponse;
import com.vdlasov.model.network.RateObject;
import com.vdlasov.repository.FixerRepository;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

/**
 * Created by Denis on 21.02.2017.
 */
public class CurrencyController {
    private final FixerRepository repo = new FixerRepository();
    private final Scanner scanner = new Scanner(System.in);

    private final Logger log = Logger.getGlobal();

    public CurrencyController() {
        System.out.println("Enter 'quit' to close program");
    }

    public void start() {
        while (true) {
            // readUserInputData
            PairCurrency inputPair = readInputDataFromUser();

            // http request
            ApiResponse currency = null;
            try {
                currency = repo.getCurrency(inputPair.getFirst(), inputPair.getSecond());
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }

            log.info(currency != null ? " Show to user this date" + currency.toString() : "no currency show to user");

            // output
            showResultToUser(currency);
        }
    }

    private boolean validateData(String input) {
        return Currency.isExist(input);
    }

    // IF user type 'quit' then destroy repository and exit
    private void checkQuitInput(String input) {
        if (input.equals("QUIT")) {
            repo.destroy();
            System.exit(0);
        }
    }

    private boolean checkEqualTwoInputs(String firstInpt, String secondInpt) {
        return firstInpt.equals(secondInpt);
    }

    private PairCurrency readInputDataFromUser() {
        System.out.println();

        // input from
        String first;
        while (true) {
            System.out.println("Enter from currency:");
            first = scanner.next().trim().toUpperCase();
            checkQuitInput(first);
            if (!validateData(first)) {
                System.out.println("Bad input from currency!");
            } else {
                break;
            }
        }

        // input to
        String second;
        while (true) {
            System.out.println("Enter to currency:");
            second = scanner.next().trim().toUpperCase();
            checkQuitInput(second);
            if (checkEqualTwoInputs(first, second) | !validateData(second)) {
                System.out.println("Bad input to currency!");
            } else {
                break;
            }
        }
        return new PairCurrency(first, second);
    }

    private void showResultToUser(ApiResponse result) {
        // check if currency equal then second will be null
        // TODO result null
        String first = result.getBase();
        RateObject rateObject = result.getRates();
        String second = rateObject != null ? rateObject.getName() : first;
        Double rate = rateObject != null ? rateObject.getRate() : 1;

        System.out.println(String.format("%s => %s : %.3f",
                first,
                second,
                rate));

        System.out.println("--------------------");
    }
}
