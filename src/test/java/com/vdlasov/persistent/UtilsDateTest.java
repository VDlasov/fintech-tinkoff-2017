package com.vdlasov.persistent;

import com.vdlasov.Utils;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by Denis on 22.02.2017.
 */
public class UtilsDateTest {
    @Test
    public void dateDiff() throws Exception {
        assertEquals(1, Utils.dateDiff("2017-02-21", "2017-02-22"));
        assertEquals(0, Utils.dateDiff("2017-02-22", "2017-02-22"));
        assertEquals(5, Utils.dateDiff("2017-02-21", "2017-02-26"));
    }

    @Test
    public void dateDiffNow() {
        Date date = new Date();
        date.setTime(date.getTime() - 1000 * 60 * 60 * 24);
        assertEquals(1, Utils.dateDiffWithNow(date));
    }
}